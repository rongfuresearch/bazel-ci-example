FROM ubuntu:20.04

# NOTE 'g++ unzip zip' are required by bazel
# RUN apt update && apt install wget g++ unzip zip

RUN \
    curl https://github.com/bazelbuild/bazelisk/releases/download/v1.14.0/bazelisk-linux-amd64 -o bazel -L && \
    mv bazel /usr/local/bin/bazel && \
    chmod +x /usr/local/bin/bazel && \
    # Unpack bazel for future use.
    bazel version

# Download some of the other tools that you explicitly need...

RUN mkdir /workspace

WORKDIR /workspace

USER bazel