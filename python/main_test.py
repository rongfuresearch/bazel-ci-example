from main import add_numbers, get_arr
import pytest

def test_add_numbers():
    assert add_numbers(1, 2) == 3

def test_get_arr():
    assert get_arr()[0] == 1

if __name__ == "__main__":
    print("start test")
    raise SystemExit(pytest.main([__file__]))